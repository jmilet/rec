defmodule Rec.WorkerUno do
	use GenServer

	# API
	def start_link(opts) do
		GenServer.start_link __MODULE__, opts, [name: {:global, opts[:name]}]
	end

	def suma(name, a, b) do
		GenServer.call({:global, name}, {:suma, a, b})
	end

	# Callbacks
	def init(opts) do
		IO.puts "Rec.WorkerUno #{opts[:name]} starting..."
		{:ok, []}
	end

	def handle_call({:suma, a, b}, _from, state) do
		{:reply, a + b, state}
	end
	
end
