defmodule Rec.SupervisorUno do
	use Supervisor

	def start_link do
		Supervisor.start_link(__MODULE__, [], [name: __MODULE__])
	end
	
	def init([]) do
		children = []

		IO.puts "Rec.SupervisorUno starting..."
		supervise(children, strategy: :one_for_one)
	end

	def supervisor_uno_child_spec(opts) do
		IO.puts "--------------"
		name = opts[:name]
		Supervisor.Spec.worker(Rec.WorkerUno, [opts], [id: name])
	end

	def supervisor_uno_desired_children do
		IO.puts "++++++++++++++"
		[[name: "uno"], [name: "dos"]]
	end

end
