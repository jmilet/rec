defmodule RecTest do
  use ExUnit.Case
  doctest Rec

  test "Rec.WorkerUno is up and running" do
		{:ok, _} = Rec.WorkerUno.start_link name: :primero
		3 = Rec.WorkerUno.suma(:primero, 1, 2)
  end

	test "Rec.SupervisorUno is up and running" do
		child_spec = Supervisor.Spec.worker(Rec.WorkerUno, [[name: :uno]], [id: :uno])

		{:ok, _} = Rec.SupervisorUno.start_link
		{:ok, pid} = Supervisor.start_child(Rec.SupervisorUno, child_spec)
		3 = Rec.WorkerUno.suma(:uno, 1, 2)

	end

	test "Grows up to three children" do
		{:ok, _} = Rec.SupervisorUno.start_link

		:ok = Populator.run Rec.SupervisorUno, &Rec.SupervisorUno.supervisor_uno_child_spec/1, &Rec.SupervisorUno.supervisor_uno_desired_children/0

		IO.inspect(:erlang.whereis(Rec.SupervisorUno) |> Supervisor.which_children)
		
	end

end
